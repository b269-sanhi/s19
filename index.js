// console .log ('Hello World')

// [SECTION] Parameters and Arguments
function printInput(){
	let nickname = prompt ('Enter your nickname: ')
	console .log('Hi, ' + nickname)
}
printInput()

// However, for some use cases, this may not be ideal
// We can use parameter and argument to pass our inputs

// Consider this function:
// (name)-> paramter
function printName(name){
	console .log('My name is ' + name)
}
// ('Pao') -> argument to be passed on teh function parameter
printName('Pao')
printName('Paowi')
printName('Elouise')

let variableName = 'Alexander'
printName(variableName)

// Create a function which can be re-used to check for a number's divisibility instead of manually doing it

function checkDivisibilityBy8(num){
	let remainder = num %8;
	console.log	('The remainder of ' + num + 'divided by 8 is: ' + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log('Is ' + num + ' divisible by 8?');
	console .log(isDivisibleby8);
} 
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as arguments
// Function parameters can also accept function arguments
// Some complex functions use other functions as arguments to perform more complicated results
// This will be further seen when we discuss array method

function argumentFunction(){
	console.log ('this function was passed as an argument')
}
argumentFunction()
function invokeFunction (x){
	argumentFunction()
}
// Adding and removing the () impacts the outputs of JS heavily
// When a functiob is used wuth (), It denotes invoke/calling
// A function used without () is normally associated with using function as an argument
invokeFunction(argumentFunction)

// Multiple parameters and arguments

function createFullName(firstName, middleName, lastName){
	console.log	(firstName + ' '+ middleName +' '+ lastName)
}
createFullName('Juan', 'Dela', 'Cruz')
createFullName('Paolo', 'Binuya', 'Sanhi')

// Providing more or less arguments than the expected parameter
createFullName('Juan', 'Dela')
createFullName('Juan', 'Dela', 'Cruz', 'Hello')
// Using variables as arguments
let firstName = 'John'
let middleName = 'Doe'
let lastName = 'Smith'
createFullName(firstName, middleName, lastName)


function printFullName (middleName, firstName, lastName){
	console.log	(firstName + ' '+ middleName +' '+ lastName)
}
printFullName('Juan', 'Dela', 'Cruz')

// Return statement
// the return statement allows us to output a value from a function to be passed to the line/block

function returnFullName(firstName, middleName, lastName){
	return firstName + ' '+ middleName + ' ' + lastName;
	// return will not allow any other line of codes to be executed below it. Kung gustong ilagay or makita yung hello eh kailangan ilagay sa itaas ng return
	console.log ('Hello')
}
// This will not display output because it is not the appropriate way to call a function return
returnFullName('Jeffrey', 'Smith', 'Bezos')
// This is the correct way
let completeName = returnFullName('Jeffrey', 'Smith', 'Bezos')
console.log(completeName)
// This also the way
console.log(returnFullName('Jeffrey', 'Smith', 'Bezos'))
// You can also create a variable inside the function to contain the result and return that the variable instead

function returnAddress (city, country){
	let fullAddress = city + ', ' + country
	return fullAddress
}
let myAddress = returnAddress('Cebu City', 'Philippines')
console.log(myAddress)

// Diong the same function calling with return using console.log
// You cannot save any value from printPlayerInfo because it does not have return anything tho gagana din naman kaso may undefined na makikita
function printPlayerInfo(username, level, job){
	console.log('Username: ' + username)
	console.log('Level: ' + level)
	console.log('Job: ' + job)
}
let user1 = printPlayerInfo('knight_white', 95 , 'Paladin')
console.log(user1)